import Image from 'next/image';
import { Header } from './components/header.js';
import Top from './components/top.js';
import Skills from './components/skills.js';
import Portfolio from './components/portfolio';
import Contact from './components/contact';
import Foot from './components/footer';

export default function Home() {
  return (
    <main>
      <div className="flex flex-col justify-between px-5 md:px-48 py-10">
        <Header />
        <Top />
        <Skills />
        <Portfolio />
      </div>
      <div className="flex flex-col justify-between px-5 md:px-48 py-10 bg-zinc-300 dark:bg-zinc-900">
        <Contact />
        <Foot />
      </div>
    </main>
  )
}
