import './globals.css'
import { mainFont } from './components/fonts';
// import { Prompt} from 'next/font/google'

// const mainFont = Prompt({subsets: ['vietnamese'], weight: '300'});

// export const mainBoldFont = Prompt({subsets: ['vietnamese'], weight: '700'})

export const metadata = {
  title: 'Claerwen Profile',
  description: 'Claerwen Profile',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={mainFont.className + " font-light"}>{children}</body>
    </html>
  )
}

module.exports;
