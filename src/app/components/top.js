export default function Top() {
    return (
        <div className="flex flex-col md:flex-row justify-between pb-20 border-b border-gray-600">
            <div className="basis-3/4 md:pr-20 flex flex-col">
                <div className="uppercase text-gray-500 my-10">Welcome</div>
                <h1 className="text-4xl md:text-6xl md:leading-snug">
                    I&apos;m Quyen, freelance  <br className="hidden md:inline" />front-end development <br className="hidden md:inline" />& graphics designer.</h1>
                <div className="my-10">
                    <button class="rounded-full text-lg 
                                    bg-gray-800 dark:bg-white hover:bg-gray-500
                                    text-white dark:text-gray-800 hover:text-white
                                    px-5 py-2 font-medium">Download CV</button>
                </div>
                <div>
                    <span class="material-symbols-outlined animate-bounce" style={{'font-size': '3rem'}}>
                        south
                    </span>
                </div>
            </div>
            <div className="md:basis-1/4 mt-10 px-20 md:px-0">
                <img src="/profile.png" className='' style={{ width: '100%', height: '' }} />
            </div>
        </div>
    );
}