import { exo2 } from './fonts.js';

function Logo() {
    return (
        <div className={exo2.className + " text-5xl flex-none md:basis-1/2 mb-5 md:mb-0"}>Q</div>           
      );
}

export function Header() {
    return (
        <div className="md:container md:mx-auto mb-5 flex-none md:flex md:flex-row md:items-center text-center md:text-left">
            <Logo/>
            <div className="flex-none md:flex md:flex-row md:basis-1/2 md:justify-end">
                <div><a href="mailto:#" className="hover:text-gray-500 dark:hover:text-gray-300">dtb.quyen.8239@gmail.com</a></div>
                <div className="hidden md:block md:mx-5">|</div>
                <div><a href="tel:#" className="hover:text-gray-500 dark:hover:text-gray-300">+84 93 767 8239</a></div>
            </div>
        </div>
    );                      
}

module.exports;