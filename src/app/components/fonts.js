import { Prompt } from 'next/font/google'
import { Exo_2 } from 'next/font/google'

export const mainFont = Prompt({subsets: ['latin'], weight: ['300', '500']});

export const exo2 = Exo_2({ 
    weight: ['400'],
    subsets: ['vietnamese']
  })


module.exports;