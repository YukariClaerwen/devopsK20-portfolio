export default function Portfolio() {
    return (
        <div className="flex flex-col md:flex-row justify-between my-20">
            <div className="basis-1/3 md:pr-20">
                <div className="uppercase text-gray-500 my-10">My Portfolio</div>

            </div>
            <div className="md:basis-2/3 mt-10 px-20 md:px-0 text-4xl">
                Comming soon...
            </div>
        </div>
    );
}