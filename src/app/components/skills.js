function ListSkill() {
    const skills = [
        { name: "Photoshop", val: 80 },
        { name: "Inlustrator", val: 60 },
        { name: "HTML/CSS", val: 90 },
        { name: "Javascript", val: 75},
        { name: "English", val: 65},
        { name: "Japanese", val: 70}
    ];
    return (
        <div>
            {
                skills.map((skill) => (
                    <div className="mb-5" key="">
                        <h2 className="mb-2">{skill.name}</h2>
                        <div class="h-1 w-full bg-neutral-200 dark:bg-neutral-600">
                            <div class="h-1 bg-gray-800 dark:bg-white" style={{ width: skill.val + '%' }}></div>
                        </div>
                    </div>
                ))
            }
        </div>
    );
}

export default function Skills() {
    return (
        <div className="flex flex-col md:flex-row justify-between my-20">
            <div className="basis-1/3 md:pr-20">
                <div className="uppercase text-gray-500 my-10">My Skills</div>

            </div>
            <div className="md:basis-2/3 mt-10 px-20 md:px-0">
                <ListSkill />
            </div>
        </div>
    );
}