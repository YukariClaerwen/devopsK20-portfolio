export default function Contact() {
    return (
        <div className="flex flex-col md:flex-row justify-between mb-20">
            <div className="basis-1/3 md:pr-20">
                <button class="rounded-full text-lg 
                                    bg-gray-800 dark:bg-white hover:bg-gray-500
                                    text-white dark:text-gray-800 hover:text-white
                                    px-5 py-2 font-medium">Get in touch</button>
            </div>
            <div className="md:basis-1/3 px-20 md:px-0">
                <div className="mb-5">E-mail</div>
                <p><a href="mailto:#" className="hover:text-gray-500 dark:hover:text-gray-300">dtb.quyen.8239@gmail.com</a></p>
            </div>
            <div className="md:basis-1/3 px-20 md:px-0">
                <div className="mb-5">Phone</div>
                <p><a href="tel:#" className="hover:text-gray-500 dark:hover:text-gray-300">+84 93 767 8239</a></p>
            </div>
        </div>
    );
}