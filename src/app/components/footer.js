export default function Foot(){
    return(
        <div className="mb-36 pt-20 border-t border-gray-600">
            <p class="rights text-primary">
                <span>©&nbsp;</span>
                <span class="copyright-year">2023</span>
                <span>.&nbsp;</span><span>Q</span>
                <span>.&nbsp;</span>
                <span>All rights reserved</span><span>.&nbsp;</span>
            </p>
        </div>
    )
}